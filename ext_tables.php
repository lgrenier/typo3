<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Dawin.' . $_EXTKEY,
	'Blog',
	'Pawney blog'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_blog';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_blog.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Pawney');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_lglgrbblog_domain_model_post', 'EXT:lg_lg_rb_blog/Resources/Private/Language/locallang_csh_tx_lglgrbblog_domain_model_post.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_lglgrbblog_domain_model_post');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_lglgrbblog_domain_model_author', 'EXT:lg_lg_rb_blog/Resources/Private/Language/locallang_csh_tx_lglgrbblog_domain_model_author.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_lglgrbblog_domain_model_author');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_lglgrbblog_domain_model_comment', 'EXT:lg_lg_rb_blog/Resources/Private/Language/locallang_csh_tx_lglgrbblog_domain_model_comment.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_lglgrbblog_domain_model_comment');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_lglgrbblog_domain_model_tag', 'EXT:lg_lg_rb_blog/Resources/Private/Language/locallang_csh_tx_lglgrbblog_domain_model_tag.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_lglgrbblog_domain_model_tag');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_lglgrbblog_domain_model_category', 'EXT:lg_lg_rb_blog/Resources/Private/Language/locallang_csh_tx_lglgrbblog_domain_model_category.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_lglgrbblog_domain_model_category');
