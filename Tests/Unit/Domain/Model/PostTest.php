<?php

namespace Dawin\LgLgRbBlog\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Lucas Garrido <lucasgarrido33@gmail.com>, Clara corp.
 *           Raya Benmessaoud <raya.benmessaoud@gmail.com>, Rillettes & co
 *           Lisa Grenier <lisag.dawin@gmail.com>, Pawney company
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Dawin\LgLgRbBlog\Domain\Model\Post.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Lucas Garrido <lucasgarrido33@gmail.com>
 * @author Raya Benmessaoud <raya.benmessaoud@gmail.com>
 * @author Lisa Grenier <lisag.dawin@gmail.com>
 */
class PostTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Dawin\LgLgRbBlog\Domain\Model\Post
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Dawin\LgLgRbBlog\Domain\Model\Post();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSummaryReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getSummary()
		);
	}

	/**
	 * @test
	 */
	public function setSummaryForStringSetsSummary()
	{
		$this->subject->setSummary('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'summary',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getContentReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getContent()
		);
	}

	/**
	 * @test
	 */
	public function setContentForStringSetsContent()
	{
		$this->subject->setContent('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'content',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPublicationDateReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getPublicationDate()
		);
	}

	/**
	 * @test
	 */
	public function setPublicationDateForDateTimeSetsPublicationDate()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setPublicationDate($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'publicationDate',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getImage()
		);
	}

	/**
	 * @test
	 */
	public function setImageForFileReferenceSetsImage()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImage($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'image',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAuteurReturnsInitialValueForAuthor()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getAuteur()
		);
	}

	/**
	 * @test
	 */
	public function setAuteurForObjectStorageContainingAuthorSetsAuteur()
	{
		$auteur = new \Dawin\LgLgRbBlog\Domain\Model\Author();
		$objectStorageHoldingExactlyOneAuteur = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneAuteur->attach($auteur);
		$this->subject->setAuteur($objectStorageHoldingExactlyOneAuteur);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneAuteur,
			'auteur',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addAuteurToObjectStorageHoldingAuteur()
	{
		$auteur = new \Dawin\LgLgRbBlog\Domain\Model\Author();
		$auteurObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$auteurObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($auteur));
		$this->inject($this->subject, 'auteur', $auteurObjectStorageMock);

		$this->subject->addAuteur($auteur);
	}

	/**
	 * @test
	 */
	public function removeAuteurFromObjectStorageHoldingAuteur()
	{
		$auteur = new \Dawin\LgLgRbBlog\Domain\Model\Author();
		$auteurObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$auteurObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($auteur));
		$this->inject($this->subject, 'auteur', $auteurObjectStorageMock);

		$this->subject->removeAuteur($auteur);

	}
}
