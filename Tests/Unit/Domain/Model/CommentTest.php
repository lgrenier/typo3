<?php

namespace Dawin\LgLgRbBlog\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Lucas Garrido <lucasgarrido33@gmail.com>, Clara corp.
 *           Raya Benmessaoud <raya.benmessaoud@gmail.com>, Rillettes & co
 *           Lisa Grenier <lisag.dawin@gmail.com>, Pawney company
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Dawin\LgLgRbBlog\Domain\Model\Comment.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Lucas Garrido <lucasgarrido33@gmail.com>
 * @author Raya Benmessaoud <raya.benmessaoud@gmail.com>
 * @author Lisa Grenier <lisag.dawin@gmail.com>
 */
class CommentTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Dawin\LgLgRbBlog\Domain\Model\Comment
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Dawin\LgLgRbBlog\Domain\Model\Comment();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTextReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getText()
		);
	}

	/**
	 * @test
	 */
	public function setTextForStringSetsText()
	{
		$this->subject->setText('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'text',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getUserNameReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getUserName()
		);
	}

	/**
	 * @test
	 */
	public function setUserNameForStringSetsUserName()
	{
		$this->subject->setUserName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'userName',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getPostReturnsInitialValueForPost()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getPost()
		);
	}

	/**
	 * @test
	 */
	public function setPostForPostSetsPost()
	{
		$postFixture = new \Dawin\LgLgRbBlog\Domain\Model\Post();
		$this->subject->setPost($postFixture);

		$this->assertAttributeEquals(
			$postFixture,
			'post',
			$this->subject
		);
	}
}
