<?php
namespace Dawin\LgLgRbBlog\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Lucas Garrido <lucasgarrido33@gmail.com>, Clara corp.
 *  			Raya Benmessaoud <raya.benmessaoud@gmail.com>, Rillettes & co
 *  			Lisa Grenier <lisag.dawin@gmail.com>, Pawney company
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Dawin\LgLgRbBlog\Controller\CommentController.
 *
 * @author Lucas Garrido <lucasgarrido33@gmail.com>
 * @author Raya Benmessaoud <raya.benmessaoud@gmail.com>
 * @author Lisa Grenier <lisag.dawin@gmail.com>
 */
class CommentControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \Dawin\LgLgRbBlog\Controller\CommentController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('Dawin\\LgLgRbBlog\\Controller\\CommentController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenCommentToCommentRepository()
	{
		$comment = new \Dawin\LgLgRbBlog\Domain\Model\Comment();

		$commentRepository = $this->getMock('Dawin\\LgLgRbBlog\\Domain\\Repository\\CommentRepository', array('add'), array(), '', FALSE);
		$commentRepository->expects($this->once())->method('add')->with($comment);
		$this->inject($this->subject, 'commentRepository', $commentRepository);

		$this->subject->createAction($comment);
	}
}
