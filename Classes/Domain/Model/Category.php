<?php
namespace Dawin\LgLgRbBlog\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Categorie
 */
class Category extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     * @validate NotEmpty
     */
    protected $name = '';
    
    /**
     * description
     *
     * @var string
     * @validate NotEmpty
     */
    protected $description = '';
    
    /**
     * post
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dawin\LgLgRbBlog\Domain\Model\Post>
     */
    protected $post = null;
    
    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->post = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Adds a Post
     *
     * @param \Dawin\LgLgRbBlog\Domain\Model\Post $post
     * @return void
     */
    public function addPost(\Dawin\LgLgRbBlog\Domain\Model\Post $post)
    {
        $this->post->attach($post);
    }
    
    /**
     * Removes a Post
     *
     * @param \Dawin\LgLgRbBlog\Domain\Model\Post $postToRemove The Post to be removed
     * @return void
     */
    public function removePost(\Dawin\LgLgRbBlog\Domain\Model\Post $postToRemove)
    {
        $this->post->detach($postToRemove);
    }
    
    /**
     * Returns the post
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dawin\LgLgRbBlog\Domain\Model\Post> $post
     */
    public function getPost()
    {
        return $this->post;
    }
    
    /**
     * Sets the post
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dawin\LgLgRbBlog\Domain\Model\Post> $post
     * @return void
     */
    public function setPost(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $post)
    {
        $this->post = $post;
    }

}