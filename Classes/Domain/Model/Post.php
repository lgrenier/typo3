<?php
namespace Dawin\LgLgRbBlog\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Post
 */
class Post extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';
    
    /**
     * summary
     *
     * @var string
     * @validate NotEmpty
     */
    protected $summary = '';
    
    /**
     * content
     *
     * @var string
     * @validate NotEmpty
     */
    protected $content = '';
    
    /**
     * publicationDate
     *
     * @var \DateTime
     * @validate NotEmpty
     */
    protected $publicationDate = null;
    
    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @validate NotEmpty
     */
    protected $image = null;
    
    /**
     * auteur
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dawin\LgLgRbBlog\Domain\Model\Author>
     * @lazy
     */
    protected $auteur = null;
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the summary
     *
     * @return string $summary
     */
    public function getSummary()
    {
        return $this->summary;
    }
    
    /**
     * Sets the summary
     *
     * @param string $summary
     * @return void
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    }
    
    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Sets the content
     *
     * @param string $content
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
    
    /**
     * Returns the publicationDate
     *
     * @return \DateTime $publicationDate
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }
    
    /**
     * Sets the publicationDate
     *
     * @param \DateTime $publicationDate
     * @return void
     */
    public function setPublicationDate(\DateTime $publicationDate)
    {
        $this->publicationDate = $publicationDate;
    }
    
    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }
    
    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }
    
    /**
     * Returns the boolean state of valid
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->valid;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->auteur = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Adds a Author
     *
     * @param \Dawin\LgLgRbBlog\Domain\Model\Author $auteur
     * @return void
     */
    public function addAuteur(\Dawin\LgLgRbBlog\Domain\Model\Author $auteur)
    {
        $this->auteur->attach($auteur);
    }
    
    /**
     * Removes a Author
     *
     * @param \Dawin\LgLgRbBlog\Domain\Model\Author $auteurToRemove The Author to be removed
     * @return void
     */
    public function removeAuteur(\Dawin\LgLgRbBlog\Domain\Model\Author $auteurToRemove)
    {
        $this->auteur->detach($auteurToRemove);
    }
    
    /**
     * Returns the auteur
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dawin\LgLgRbBlog\Domain\Model\Author> $auteur
     */
    public function getAuteur()
    {
        return $this->auteur;
    }
    
    /**
     * Sets the auteur
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Dawin\LgLgRbBlog\Domain\Model\Author> $auteur
     * @return void
     */
    public function setAuteur(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $auteur)
    {
        $this->auteur = $auteur;
    }

}