<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "lg_lg_rb_blog"
 *
 * Auto generated by Extension Builder 2016-01-11
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Pawney',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Lucas Garrido, Raya Benmessaoud, Lisa Grenier',
	'author_email' => 'lucasgarrido33@gmail.com, raya.benmessaoud@gmail.com, lisag.dawin@gmail.com',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);